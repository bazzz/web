module gitlab.com/bazzz/web

go 1.16

require (
	github.com/google/uuid v1.2.0
	github.com/muesli/cache2go v0.0.0-20201208071950-e3e970b4892f
)
