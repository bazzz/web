package web

// Controller is an interface for all web controllers.
type Controller interface {
	Get(request Request) Response
	Post(request Request) Response
}

// RedirectPostToGet is a controller template to compose into controllers that have no Post handler. The default post handler will redirect to the Get handler.
type RedirectPostToGet struct {
}

// Post is the default Post handler for the RedirectPostToGet that redirects to the Get handler.
func (RedirectPostToGet) Post(request Request) (response Response) {
	response.RedirectURL = request.Pattern
	return
}
