package web

import (
	"errors"
	"html/template"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/muesli/cache2go"
)

// TemplateExtension defines which files in webRoot classify as go html templates.
var TemplateExtension = ".gohtml"

// SessionCookieName is the name used for the session cookie.
const SessionCookieName = "SID"

var logger Logger = newDefaultLogger()

// Server defines a default http webserver.
type Server struct {
	execPath        string
	httpServer      *http.Server
	serveMux        *http.ServeMux
	templates       *template.Template
	webRoot         string
	sessions        *cache2go.CacheTable
	assetDirs       []string
	noCacheExts     map[string]bool
	funcs           map[string]func(input string) interface{}
	SessionDuration time.Duration
	CertificatePath string
	KeyPath         string
}

// New returns a Server serving host:port from the relative path at webRoot. All subdirectories of webRoot are considered assets and will be registered as assetpaths to be served statically.
func New(host string, port int, webRoot string) (*Server, error) {
	exec, err := os.Executable()
	if err != nil {
		return nil, err
	}
	execPath := filepath.Dir(exec)
	if filepath.IsAbs(webRoot) {
		return nil, errors.New("webroot must be relative")
	}
	serveMux := http.NewServeMux()
	httpServer := &http.Server{
		Addr:    host + ":" + strconv.Itoa(port),
		Handler: serveMux,
	}
	assetDirs, err := getSubDirs(webRoot)
	if err != nil {
		return nil, err
	}
	webServer := Server{
		execPath:    execPath,
		httpServer:  httpServer,
		serveMux:    serveMux,
		webRoot:     webRoot,
		sessions:    cache2go.Cache("sessions"),
		assetDirs:   assetDirs,
		noCacheExts: make(map[string]bool),
		funcs:       make(map[string]func(input string) interface{}),
	}
	return &webServer, nil
}

// SetLogger makes the server use the provided logger instead of the default logger from the Go standard packages.
func SetLogger(newLogger Logger) {
	logger = newLogger
}

// Start enables the webserver.
func (s *Server) Start() error {
	err := os.MkdirAll(s.webRoot, os.ModePerm)
	if err != nil {
		return err
	}
	err = s.createFileServer()
	if err != nil {
		return err
	}

	funcMap := template.FuncMap{}
	for name, function := range s.funcs {
		funcMap[name] = function
	}
	templatesPattern := filepath.Join(s.execPath, s.webRoot, "*"+TemplateExtension)
	templates := template.Must(template.New("").Funcs(funcMap).ParseGlob(templatesPattern))
	s.templates = templates

	if s.CertificatePath != "" && s.KeyPath != "" {
		return s.httpServer.ListenAndServeTLS(s.CertificatePath, s.KeyPath)
	}
	return s.httpServer.ListenAndServe()
}

// AddAssetsPath adds a path to a directory containing files to be served as assets. Note that all subdirectories of webRoot are considered assets by default and do not to be added manually.
func (s *Server) AddAssetsPath(path string) {
	s.assetDirs = append(s.assetDirs, path)
}

// AddNoCacheExt adds the given file extension to the no cache extensions. Any static file served with such extention will have header "Cache-Control = no-store, max-age=0".
func (s *Server) AddNoCacheExt(ext string) {
	s.noCacheExts[ext] = true
}

// AddTemplateFunction adds the custom function to to template engine so it can be called from templates. The function should probably return someting that the template engine can render to html including things like template.HTML or template.URL or just string.
func (s *Server) AddTemplateFunction(name string, function func(input string) interface{}) {
	s.funcs[name] = function
}

// Register adds a controller to an url pattern.
func (s *Server) Register(pattern string, controller Controller) {
	s.register(pattern, controller.Get, controller.Post)
}

// RegisterGenericHandler registers a handler to an url pattern, without a controller, view and bag.
func (s *Server) RegisterGenericHandler(pattern string, handler func(request Request) (data []byte, contentType string)) {
	s.serveMux.HandleFunc(pattern, func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "GET" && r.Method != "POST" {
			logger.Error("webserver Generic Handler for", r.Method, "not implemented.")
		}
		request := s.getRequest(pattern, r)
		if r.Method == "POST" {
			postData, err := newPost(r)
			if err != nil {
				logger.Error("webserver handler for ", r.URL.String(), " could not parse post data: ", err)
			}
			request.post = postData
		}
		data, contentType := handler(request)
		if contentType != "" {
			w.Header().Set("Content-Type", contentType)
		}
		_, err := w.Write(data)
		if err != nil {
			logger.Error(err)
		}
	})
}

// RegisterFileHandler registers a handler that serves files from path for an url pattern.
func (s *Server) RegisterFileHandler(pattern string, path string, handlerFunc func(Request, http.Handler) http.Handler) {
	s.serveMux.HandleFunc(pattern, func(w http.ResponseWriter, r *http.Request) {
		fileServer := http.FileServer(http.Dir(path))
		fileHandler := http.StripPrefix(pattern, fileServer)
		request := s.getRequest(pattern, r)
		url := r.URL.String()
		if strings.Contains(url, ".") {
			ext := url[strings.LastIndex(url, ".")+1:]
			if ok := s.noCacheExts[ext]; ok {
				w.Header().Set("Cache-Control", "no-store, max-age=0")
			}
		}
		handlerFunc(request, fileHandler).ServeHTTP(w, r)
	})
}

// RegisterCustomHandler registers a custom http.Handler to this server's servemux. This is useful to allow this server to serve "other things" over HTTP instead of webpages, for instance websockets. If possible consider using RegisterGenericHandler instead.
func (s *Server) RegisterCustomHandler(pattern string, handler http.Handler) {
	s.serveMux.Handle(pattern, handler)
}

func (s *Server) createFileServer() error {
	for _, assetDir := range s.assetDirs {
		filePath := ""
		label := ""
		if !filepath.IsAbs(assetDir) {
			filePath = filepath.Join(s.execPath, s.webRoot, assetDir)
			label = filepath.Join(s.webRoot, assetDir)
		} else {
			filePath = assetDir
			assetDir = filepath.Base(assetDir)
			label = assetDir
		}
		err := os.MkdirAll(filePath, os.ModePerm)
		if err != nil {
			return err
		}
		prefix := "/" + strings.ReplaceAll(label, "\\", "/") + "/"
		fserver := http.FileServer(http.Dir(filePath))
		handler := s.fileServeHandler(http.StripPrefix(prefix, fserver))
		s.serveMux.Handle(prefix, handler)
	}
	return nil
}

func (s *Server) fileServeHandler(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_ = s.GetSession(r) // Only creating/updating session. No need to do anything with it.
		url := r.URL.String()
		if strings.Contains(url, ".") {
			ext := url[strings.LastIndex(url, ".")+1:]
			if ok := s.noCacheExts[ext]; ok {
				w.Header().Set("Cache-Control", "no-store, max-age=0")
			}
		}
		handler.ServeHTTP(w, r)
	})
}

func (s *Server) register(pattern string, get func(Request) Response, post func(Request) Response) {
	s.serveMux.HandleFunc(pattern, func(w http.ResponseWriter, r *http.Request) {
		if r.URL.String() == "/favicon.ico" {
			w.WriteHeader(http.StatusGone) // Why do browsers still ask this?
			return
		}
		if pattern == "/" && r.URL.String() != "/" { // pattern "/" matches anything that did not match another pattern so the URL may be different, this is an invalid state though.
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}
		request := s.getRequest(pattern, r)
		var response Response
		switch r.Method {
		case "GET":
			response = get(request)
		case "POST":
			if post == nil {
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			postData, err := newPost(r)
			if err != nil {
				logger.Error("webserver handler for ", r.URL.String(), " could not parse post data: ", err)
			}
			request.post = postData
			response = post(request)
		default:
			return
		}
		if s.SessionDuration > 0 {
			s.setSessionCookie(w, request.session.ID)
		}
		response.CookieJar().Write(w)

		if response.RedirectURL != "" {
			http.Redirect(w, r, response.RedirectURL, http.StatusFound)
			return
		}
		if response.ViewName == "" {
			w.WriteHeader(http.StatusNotFound)
			return
		}
		err := s.templates.ExecuteTemplate(w, response.ViewName, response.Bag())
		if err != nil {
			if err.Error() != "http2: stream closed" { // Bug in http server?
				logger.Error("webserver could not execute", response.ViewName, "template:", err)
			}
		}
	})
}

func (s *Server) setSessionCookie(w http.ResponseWriter, sessionID string) {
	cookie := http.Cookie{
		Name:     SessionCookieName,
		Value:    sessionID,
		Path:     "/",
		SameSite: http.SameSiteStrictMode,
	}
	http.SetCookie(w, &cookie)
}

func (s *Server) getRequest(pattern string, r *http.Request) (request Request) {
	request = Request{
		Pattern:       pattern,
		values:        strings.Split(r.URL.Path[len(pattern):], "/"),
		Query:         map[string][]string(r.URL.Query()),
		host:          r.Host,
		remoteAddress: r.RemoteAddr,
		useragent:     r.Header.Get("User-Agent"),
		session:       s.GetSession(r),
		cookies:       newCookieJar(r),
	}
	return
}

// GetSession returns a session object created from the http request r if it has a session ID cookie.
func (s *Server) GetSession(r *http.Request) *Session {
	if s.SessionDuration <= 0 {
		return nil
	}
	cookie, err := r.Cookie(SessionCookieName)
	if err != nil && err != http.ErrNoCookie {
		logger.Error("webserver could not read session cookie:", err)
		return nil
	}
	if cookie != nil {
		sessionID := cookie.Value
		cacheItem, err := s.sessions.Value(sessionID)
		if err == nil {
			return cacheItem.Data().(*Session)
		}
	}
	sessionID := uuid.New()
	session := Session{
		Bag:       &Bag{},
		ID:        sessionID.String(),
		updatedAt: time.Now(),
	}
	s.sessions.Add(sessionID.String(), s.SessionDuration, &session)
	return &session
}
