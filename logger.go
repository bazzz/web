package web

import "log"

// Logger is an interface for any object that provides a way to store error messages.
type Logger interface {
	Error(text ...interface{})
}

func newDefaultLogger() *defaultLogger {
	logger := defaultLogger{
		logger: log.Default(),
	}
	return &logger
}

type defaultLogger struct {
	logger *log.Logger
}

func (l *defaultLogger) Error(text ...interface{}) {
	l.logger.Println(text...)
}
