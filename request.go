package web

// Request represents a http request.
type Request struct {
	Pattern       string
	Query         map[string][]string
	post          *Post
	values        []string
	host          string
	remoteAddress string
	useragent     string
	session       *Session
	cookies       *CookieJar
}

// Post returns the post data container contained in this request.
func (r Request) Post() *Post {
	return r.post
}

// Values returns a collection of strings in the query string that were found after the pattern.
func (r Request) Values() []string {
	return r.values
}

// Session returns the session this request belongs to if the webserver is enabled to use sessions, otherwise nil.
func (r Request) Session() *Session {
	return r.session
}

// Host returns the hostname against which the request was made.
func (r Request) Host() string {
	return r.host
}

// RemoteAddress returns the address of the requester in the ip:port format.
func (r Request) RemoteAddress() string {
	return r.remoteAddress
}

// UserAgent returns the user-agent string as provided by the http request.
func (r Request) UserAgent() string {
	return r.useragent
}

// CookieJar returns a collection of the cookies found in the http request.
func (r Request) CookieJar() *CookieJar {
	return r.cookies
}
