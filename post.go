package web

import (
	"io"
	"net/http"
)

// Post represents a container for post data from a web request.
type Post struct {
	request *http.Request
}

func newPost(request *http.Request) (*Post, error) {
	err := request.ParseMultipartForm(128 << 20) // 128MB
	if err != nil {
		return nil, err
	}
	post := Post{
		request: request,
	}
	return &post, nil
}

// Names returns a collection of all names in the post data.
func (p *Post) Names() []string {
	keys := make([]string, len(p.request.Form))
	i := 0
	for key := range p.request.Form {
		keys[i] = key
		i++
	}
	return keys
}

// Get returns the value from the post data by name.
func (p *Post) Get(name string) string {
	return p.request.FormValue(name)
}

// GetFile returns the filename and an io.Reader from the post data by name. If the filename is an empty string, no file was uploaded.
func (p *Post) GetFile(name string) (filename string, data io.Reader) {
	file, header, err := p.request.FormFile(name)
	if err != nil {
		if err.Error() == http.ErrMissingFile.Error() {
			// ignore missing file case.
			return
		}
		errorText := err.Error()
		if p.Get(name) != "" {
			errorText += ", but found a text for the same name, make sure the <form> has enctype=\"multipart/form-data\""
		}
		logger.Error(errorText)
		return "", nil
	}
	return header.Filename, file
}
