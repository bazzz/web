package web

// Bag is a name/value collection.
type Bag map[string]interface{}

// Set stores the provided value under the name.
func (b Bag) Set(name string, value interface{}) {
	b[name] = value
}

// Get returns the value by name.
func (b Bag) Get(name string) interface{} {
	if val, ok := b[name]; ok {
		return val
	}
	return nil
}

// GetInteger returns the value by name and cast to an integer. If the value does not exist or cannot be cast to int it returns 0.
func (b Bag) GetInteger(name string) int {
	value := b.Get(name)
	if integer, ok := value.(int); ok {
		return integer
	}
	return 0
}

// GetBoolean returns the value by name and cast to a boolean. If the value does not exist or cannot be cast to bool it returns false.
func (b Bag) GetBoolean(name string) bool {
	value := b.Get(name)
	if boolean, ok := value.(bool); ok {
		return boolean
	}
	return false
}
