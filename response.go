package web

// Response represents a http web response. Set ViewName to have this response serve a view.
type Response struct {
	ViewName    string
	RedirectURL string
	bag         Bag
	cookies     *CookieJar
}

func (r *Response) Bag() Bag {
	if r.bag == nil {
		r.bag = Bag(make(map[string]interface{}))
	}
	return r.bag
}

// CookieJar returns a collection of the cookies found in the http request.
func (r *Response) CookieJar() *CookieJar {
	if r.cookies == nil {
		r.cookies = newCookieJar(nil)
	}
	return r.cookies
}
