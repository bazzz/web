package web

import "time"

// Session is a holder for session information.
type Session struct {
	*Bag
	ID        string
	updatedAt time.Time
}
