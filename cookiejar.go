package web

import (
	"net/http"
	"time"
)

// CookieJar represents a holder for name/values in the http cookies.
type CookieJar struct {
	jar []*http.Cookie
}

func newCookieJar(r *http.Request) *CookieJar {
	cookieJar := CookieJar{
		jar: make([]*http.Cookie, 0),
	}
	if r != nil {
		cookieJar.jar = r.Cookies()
	}
	return &cookieJar
}

// Get returns the value for the cookie with the specified name from the Request, or an empty string if no such cookie was found.
func (j *CookieJar) Get(name string) string {
	for _, cookie := range j.jar {
		if cookie.Name == name {
			return cookie.Value
		}
	}
	return ""
}

// Set adds a cookie with the provided name and value to the Response.
func (j *CookieJar) Set(name string, value string) {
	cookie := http.Cookie{
		Name:     name,
		Value:    value,
		Path:     "/",
		Expires:  time.Now().AddDate(0, 1, 0),
		HttpOnly: true,
		SameSite: http.SameSiteLaxMode,
	}
	j.jar = append(j.jar, &cookie)
}

// Write writes the cookies to the Responsewriter.
func (j *CookieJar) Write(w http.ResponseWriter) {
	for _, cookie := range j.jar {
		if cookie.Value == "" {
			cookie.Expires = time.Now()
		}
		http.SetCookie(w, cookie)
	}
}
