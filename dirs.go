package web

import "io/ioutil"

func getSubDirs(dir string) ([]string, error) {
	dirs := make([]string, 0)
	infos, err := ioutil.ReadDir(dir)
	if err != nil {
		return dirs, err
	}
	for _, info := range infos {
		if !info.IsDir() {
			continue
		}
		dirs = append(dirs, info.Name())
	}
	return dirs, nil
}
